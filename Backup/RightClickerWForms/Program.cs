﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using MouseKeyboardLibrary;

namespace RightClickerWForms
{
    

    static class Program
    {        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            KeyboardHook _kHook = new KeyboardHook();
            _kHook.Start();
            _kHook.KeyUp += new KeyEventHandler(_kHook_KeyUp);
            _kHook.KeyDown += new KeyEventHandler(_kHook_KeyDown);

            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormDummy());            
        }

        static void _kHook_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.RWin)
            {
                e.Handled = true;
            }
        }


        static void _kHook_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.RWin)
            {
                e.Handled = true;
                System.Threading.Thread.Sleep(200);
                MouseSimulator.Click(MouseButton.Right);                
            }
        }
    }
}
