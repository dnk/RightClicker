﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows;

using MouseKeyboardLibrary;

namespace RightClickerWForms
{
    

    static class Program
    {        
        static Alphabet _alpha = new Alphabet();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            KeyboardHook _kHook = new KeyboardHook();
            _kHook.Start();
            _kHook.KeyUp += new KeyEventHandler(_kHook_KeyUp);
            _kHook.KeyDown += new KeyEventHandler(_kHook_KeyDown);

            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormDummy());            
        }

        static void _kHook_KeyDown(object sender, KeyEventArgs e)
        {
            Trace.WriteLine(e.KeyCode);

            if (e.KeyCode == Keys.RWin)
            {
                e.Handled = true;
            }
            if (e.KeyCode == Keys.RMenu)
            {
                e.Handled = true;
            }
        }


        static void _kHook_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.RWin)
            {
                e.Handled = true;
                System.Threading.Thread.Sleep(200);
                MouseSimulator.Click(MouseButton.Right);                
            }

            if (e.KeyCode == Keys.RMenu)
            {
                e.Handled = true;
                //System.Threading.Thread.Sleep(200);
                System.Windows.IDataObject clObj = null;
                try
                {
                    // Сохраняем клипборд
                    //clObj = System.Windows.Clipboard.GetDataObject();

                    KeyboardSimulator.SimulateStandardShortcut(StandardShortcut.Cut);
                    Trace.WriteLine("Вырез");
                    bool haveText = false;

                    Thread t = new Thread(new ThreadStart(() =>
                        {
                            string text = "";
                            // Надо пытаться несколько раз, т.к. КБ может быть открыт где-то еще
                            const int maxHops = 200;
                            for (int i = 0; i < maxHops; i++)
                            {
                                try
                                {
                                    System.Threading.Thread.Sleep(10);
                                    
                                    text = System.Windows.Clipboard.GetText();
                                    Trace.WriteLine("Текст получен");
                                    break;
                                }
                                catch (Exception ex)
                                {
                                    // Последняя попытка не удалась
                                    if (i == maxHops - 1)
                                    {
                                        Trace.WriteLine("Неудача");
                                        throw new Exception(ex.ToString());
                                    }
                                }
                            }
                            // Транскод клипбоарда
                            if (!string.IsNullOrEmpty(text))
                            {
                                haveText = true;
                                text = _alpha.Transcode(text);

                                // Суем текст обратно
                                for (int i = 0; i < maxHops; i++)
                                {
                                    try
                                    {
                                        System.Threading.Thread.Sleep(10);

                                        System.Windows.Clipboard.SetText(text);
                                        Trace.WriteLine("Текст отправлен в клипборд");
                                        break;
                                    }
                                    catch (Exception ex)
                                    {
                                        // Последняя попытка не удалась
                                        if (i == maxHops - 1)
                                        {
                                            Trace.WriteLine("Неудача вставки текста");
                                            throw new Exception(ex.ToString());
                                        }
                                    }
                                }
                                
                            }
                        }));
                    t.SetApartmentState(ApartmentState.STA);
                    t.Start();

                    t.Join();

                    if (haveText)
                    {
                        Thread.Sleep(100);
                        KeyboardSimulator.SimulateStandardShortcut(StandardShortcut.Paste);
                        Trace.WriteLine("Вставка");

                        // Теперь мы поменяем раскладку
                        KeyboardSimulator.KeyDown(Keys.LWin);
                        KeyboardSimulator.KeyPress(Keys.Space);
                        KeyboardSimulator.KeyUp(Keys.LWin);
                    }
                    // Возвращаем все назад в клипборде
                    //System.Windows.Clipboard.SetDataObject(clObj);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.ToString());

                    // Отмена вырезания
                    //KeyboardSimulator.KeyDown(Keys.Control);
                    //KeyboardSimulator.KeyPress(Keys.Z);
                    //KeyboardSimulator.KeyUp(Keys.Control);
                }
            }
        }
    }
}
